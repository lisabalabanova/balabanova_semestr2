#ifndef USERSUI_H
#define USERSUI_H

#include <QDialog>

namespace Ui {
class UsersUi;
}

class UsersUi : public QDialog
{
    Q_OBJECT

public:
    explicit UsersUi(QWidget *parent = nullptr);
    ~UsersUi();

private slots:
    void on_pushButton_clicked();

private:
    Ui::UsersUi *ui;
};

#endif // USERSUI_H
