#include "committees_user.h"
#include "ui_committees_user.h"
#include "menu_user.h"
#include <QFile>

Committees_user::Committees_user(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Committees_user)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/committees.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

Committees_user::~Committees_user()
{
    delete ui;
}

void Committees_user::on_pushButton_clicked()
{
    hide();
    Menu_user menu_user_window;
    menu_user_window.setModal(true);
    menu_user_window.exec();
}

void Committees_user::on_pushButton_2_clicked()
{
    ui->label->setText(ui->listWidget->currentItem()->text());
}
