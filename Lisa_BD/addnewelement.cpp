#include "addnewelement.h"
#include "ui_addnewelement.h"

AddNewElement::AddNewElement(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddNewElement)
{
    ui->setupUi(this);
}

AddNewElement::~AddNewElement()
{
    delete ui;
}
