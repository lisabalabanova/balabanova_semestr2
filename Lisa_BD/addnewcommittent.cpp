#include "addnewcommittent.h"
#include "ui_addnewcommittent.h"
#include "committees.cpp"
#include <QMessageBox>
#include "committees_admin.h"


AddNewCommittent::AddNewCommittent(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddNewCommittent)
{
    ui->setupUi(this);
}

AddNewCommittent::~AddNewCommittent()
{
    delete ui;
}

void AddNewCommittent::on_pushButton_clicked()
{
    QString name = ui->name->text();
    QString surname = ui->surname->text();

    Committent new_element = Committent(name, surname);

    new_element.add_in_db();

    QMessageBox::information(this, "Add element", "Done!");
    ui->name->setText("");
    ui->surname->setText("");
}

void AddNewCommittent::on_pushButton_2_clicked()
{
    hide();
    Committees_admin committint_admin_window;
    committint_admin_window.setModal(true);
    committint_admin_window.exec();
}
