#include "menu.h"
#include "ui_menu.h"
#include "thirdwindow.h"
#include "mainwindow.h"
#include "committees_admin.h"
#include "journals_admin.h"


Menu::Menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Menu)
{
    ui->setupUi(this);
}

Menu::~Menu()
{
    delete ui;
}

void Menu::on_pushButton_clicked()
{
    hide();
    ThirdWindow admin_window;
    admin_window.setModal(true);
    admin_window.exec();
}

void Menu::on_pushButton_2_clicked()
{
    hide();
    Committees_admin committees_admin_window;
    committees_admin_window.setModal(true);
    committees_admin_window.exec();
}

void Menu::on_pushButton_3_clicked()
{
    hide();
    Journals_admin journal_admin_window;
    journal_admin_window.setModal(true);
    journal_admin_window.exec();
}

void Menu::on_pushButton_4_clicked()
{
    qApp->quit();
}
