#include "menu_user.h"
#include "ui_menu_user.h"
#include "secondwindow.h"
#include "mainwindow.h"
#include "committees_user.h"
#include "journals.h"

Menu_user::Menu_user(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Menu_user)
{
    ui->setupUi(this);
}

Menu_user::~Menu_user()
{
    delete ui;
}

void Menu_user::on_pushButton_clicked()
{
    hide();
    SecondWindow user_window;
    user_window.setModal(true);
    user_window.exec();
}

void Menu_user::on_pushButton_2_clicked()
{
    hide();
    Committees_user committees_user_window;
    committees_user_window.setModal(true);
    committees_user_window.exec();
}

void Menu_user::on_pushButton_3_clicked()
{
    hide();
    Journals journal_user_window;
    journal_user_window.setModal(true);
    journal_user_window.exec();
}

void Menu_user::on_pushButton_4_clicked()
{
    qApp->quit();
}
