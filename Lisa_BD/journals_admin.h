#ifndef JOURNALS_ADMIN_H
#define JOURNALS_ADMIN_H

#include <QDialog>

namespace Ui {
class Journals_admin;
}

class Journals_admin : public QDialog
{
    Q_OBJECT

public:
    explicit Journals_admin(QWidget *parent = 0);
    ~Journals_admin();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Journals_admin *ui;
};

#endif // JOURNALS_ADMIN_H
