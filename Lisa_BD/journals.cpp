#include "journals.h"
#include "ui_journals.h"
#include "menu_user.h"
#include <QFile>

Journals::Journals(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Journals)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/rent.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }

    QFile file2("C://Users/Lisanus/Desktop/projects/Lisa/buy.txt");
    if(file2.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file2.atEnd())
        {
            //читаем строку
            QString str = file2.readLine();
            ui->listWidget_2->addItem(str);
        }

    }
}

Journals::~Journals()
{
    delete ui;
}

void Journals::on_pushButton_clicked()
{
    hide();
    Menu_user menu_user_window;
    menu_user_window.setModal(true);
    menu_user_window.exec();
}
