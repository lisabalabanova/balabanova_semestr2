#ifndef JOURNALS_H
#define JOURNALS_H

#include <QDialog>

namespace Ui {
class Journals;
}

class Journals : public QDialog
{
    Q_OBJECT

public:
    explicit Journals(QWidget *parent = 0);
    ~Journals();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Journals *ui;
};

#endif // JOURNALS_H
