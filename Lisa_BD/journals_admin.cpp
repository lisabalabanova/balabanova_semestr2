#include "journals_admin.h"
#include "ui_journals_admin.h"
#include "menu.h"
#include <QFile>

Journals_admin::Journals_admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Journals_admin)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/rent.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }

    QFile file2("C://Users/Lisanus/Desktop/projects/Lisa/buy.txt");
    if(file2.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file2.atEnd())
        {
            //читаем строку
            QString str = file2.readLine();
            ui->listWidget_2->addItem(str);
        }

    }
}

Journals_admin::~Journals_admin()
{
    delete ui;
}

void Journals_admin::on_pushButton_clicked()
{
    hide();
    Menu menu_admin_window;
    menu_admin_window.setModal(true);
    menu_admin_window.exec();
}
