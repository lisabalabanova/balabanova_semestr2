#ifndef MENU_USER_H
#define MENU_USER_H

#include <QDialog>

namespace Ui {
class Menu_user;
}

class Menu_user : public QDialog
{
    Q_OBJECT

public:
    explicit Menu_user(QWidget *parent = 0);
    ~Menu_user();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::Menu_user *ui;
};

#endif // MENU_USER_H
