#ifndef ADDNEWCOMMITTENT_H
#define ADDNEWCOMMITTENT_H

#include <QDialog>

namespace Ui {
class AddNewCommittent;
}

class AddNewCommittent : public QDialog
{
    Q_OBJECT

public:
    explicit AddNewCommittent(QWidget *parent = 0);
    ~AddNewCommittent();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::AddNewCommittent *ui;
};

#endif // ADDNEWCOMMITTENT_H
