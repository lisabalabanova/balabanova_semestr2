#include "addnew.h"
#include "ui_addnew.h"
#include "thirdwindow.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include "jewel.cpp"

AddNew::AddNew(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddNew)
{
    ui->setupUi(this);
}

AddNew::~AddNew()
{
    delete ui;
}

void AddNew::on_pushButton_2_clicked()
{
    hide();
    ThirdWindow admin_window;
    admin_window.setModal(true);
    admin_window.exec();
}

void AddNew::on_pushButton_clicked()
{
    QString name = ui->name->text();
    QString price = ui->price->text();

    Jewel new_element = Jewel(name, price);

    new_element.add_in_db();

    QMessageBox::information(this, "Add element", "Done!");
    ui->name->setText("");
    ui->price->setText("");
}
