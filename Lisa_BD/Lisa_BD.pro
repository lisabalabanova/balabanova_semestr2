#-------------------------------------------------
#
# Project created by QtCreator 2019-03-20T21:41:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lisa
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    secondwindow.cpp \
    thirdwindow.cpp \
    addnew.cpp \
    jewel.cpp \
    menu.cpp \
    menu_user.cpp \
    committees.cpp \
    committees_admin.cpp \
    addnewcommittent.cpp \
    committees_user.cpp \
    journals.cpp \
    journals_admin.cpp

HEADERS += \
        mainwindow.h \
    secondwindow.h \
    thirdwindow.h \
    addnew.h \
    menu.h \
    menu_user.h \
    committees_admin.h \
    addnewcommittent.h \
    committees_user.h \
    journals.h \
    journals_admin.h

FORMS += \
        mainwindow.ui \
    secondwindow.ui \
    thirdwindow.ui \
    addnew.ui \
    menu.ui \
    menu_user.ui \
    committees_admin.ui \
    addnewcommittent.ui \
    committees_user.ui \
    journals.ui \
    journals_admin.ui
