#include "thirdwindow.h"
#include "ui_thirdwindow.h"
#include "menu.h"
#include "addnew.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <string>
#include <fstream>
using namespace std;
ThirdWindow::ThirdWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ThirdWindow)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/jewelery.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

ThirdWindow::~ThirdWindow()
{
    delete ui;
}

void ThirdWindow::on_pushButton_clicked()
{
    ui->label->setText(ui->listWidget->currentItem()->text());
}

void ThirdWindow::on_pushButton_2_clicked()
{
    hide();
    AddNew add_new_window;
    add_new_window.setModal(true);
    add_new_window.exec();
}

void ThirdWindow::on_pushButton_3_clicked()
{
 /*   QString delete_str = ui->listWidget->currentItem()->text();
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/jewelery.txt");
    QStringList strList;
    //Считываем исходный файл в контейнер
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        while(!file.atEnd())
        {
        strList << file.readLine();
        }
        file.close();
    }
*/

        QListWidgetItem *it = ui->listWidget->item(ui->listWidget->currentRow());
          delete it;
        QStringList myStl;
        int count = ui->listWidget->count();
        for(int index = 0; index < count; index++)
        {
        QListWidgetItem * item = ui->listWidget->item(index);
        if(item->text().length()> 4)
        myStl << item->text();
        }
        QString list1 = myStl.at(0);
       string text = list1.toUtf8().constData();
       ofstream outt("C://Users/Lisanus/Desktop/projects/Lisa/jewelery.txt", fstream::in | fstream::trunc  | std::fstream::binary );
        if(outt.is_open()){
           outt<< text;
        }
        outt.close();

}

void ThirdWindow::on_pushButton_4_clicked()
{
    hide();
    Menu menu_window;
    menu_window.setModal(true);
    menu_window.exec();
}

void ThirdWindow::on_pushButton_5_clicked()
{
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/rent.txt");
    QString str = ui->listWidget->currentItem()->text();
    QByteArray ba_str = str.toLocal8Bit();
    const char *c_str = ba_str.data();

    QStringList strList;
    /*Считываем исходный файл в контейнер*/
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        while(!file.atEnd())
        {
        strList << file.readLine();
        }
        file.close();
    }
    /*Добавляем строку и сохраняем содержимое контейнера в тот же файл*/
    if ((file.exists())&&(file.open(QIODevice::WriteOnly)))
    {
        strList.append(c_str);
        QTextStream stream(&file);
        foreach(QString s, strList)
        {
            stream<<s;
        }
        file.close();
    }
    QMessageBox::information(this, "Add element", "Done!");
}

void ThirdWindow::on_pushButton_6_clicked()
{
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/buy.txt");
    QString str = ui->listWidget->currentItem()->text();
    QByteArray ba_str = str.toLocal8Bit();
    const char *c_str = ba_str.data();

    QStringList strList;
    /*Считываем исходный файл в контейнер*/
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        while(!file.atEnd())
        {
        strList << file.readLine();
        }
        file.close();
    }
    /*Добавляем строку и сохраняем содержимое контейнера в тот же файл*/
    if ((file.exists())&&(file.open(QIODevice::WriteOnly)))
    {
        strList.append(c_str);
        QTextStream stream(&file);
        foreach(QString s, strList)
        {
            stream<<s;
        }
        file.close();
    }
    QMessageBox::information(this, "Add element", "Done!");
}
