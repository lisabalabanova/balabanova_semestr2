#ifndef COMMITTEES_USER_H
#define COMMITTEES_USER_H

#include <QDialog>

namespace Ui {
class Committees_user;
}

class Committees_user : public QDialog
{
    Q_OBJECT

public:
    explicit Committees_user(QWidget *parent = 0);
    ~Committees_user();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Committees_user *ui;
};

#endif // COMMITTEES_USER_H
