#ifndef COMMITTEES_ADMIN_H
#define COMMITTEES_ADMIN_H

#include <QDialog>

namespace Ui {
class Committees_admin;
}

class Committees_admin : public QDialog
{
    Q_OBJECT

public:
    explicit Committees_admin(QWidget *parent = 0);
    ~Committees_admin();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Committees_admin *ui;
};

#endif // COMMITTEES_ADMIN_H
