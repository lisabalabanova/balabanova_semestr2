#include "committees_admin.h"
#include "ui_committees_admin.h"
#include <QFile>
#include "addnewcommittent.h"
#include "menu.h"
#include <fstream>
#include <string>
using namespace std;
Committees_admin::Committees_admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Committees_admin)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/committees.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

Committees_admin::~Committees_admin()
{
    delete ui;
}

void Committees_admin::on_pushButton_clicked()
{
    ui->label->setText(ui->listWidget->currentItem()->text());
}

void Committees_admin::on_pushButton_2_clicked()
{
    hide();
    AddNewCommittent add_new_committent_window;
    add_new_committent_window.setModal(true);
    add_new_committent_window.exec();
}

void Committees_admin::on_pushButton_4_clicked()
{
    hide();
    Menu menu_admin_window;
    menu_admin_window.setModal(true);
    menu_admin_window.exec();
}

void Committees_admin::on_pushButton_3_clicked()
{
    QListWidgetItem *it = ui->listWidget->item(ui->listWidget->currentRow());
      delete it;
    QStringList myStl;
    int count = ui->listWidget->count();
    for(int index = 0; index < count; index++)
    {
    QListWidgetItem * item = ui->listWidget->item(index);
    if(item->text().length()> 4)
    myStl << item->text();
    }
    QString list1 = myStl.at(0);
    string text = list1.toUtf8().constData();
    ofstream outt("C://Users/Lisanus/Desktop/projects/Lisa/committees.txt", fstream::in | fstream::trunc  | std::fstream::binary );
    if(outt.is_open()){
       outt<< text;
    }
    outt.close();

}
