#include "secondwindow.h"
#include "ui_secondwindow.h"
#include <QFile>
#include "menu_user.h"

SecondWindow::SecondWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SecondWindow)
{
    ui->setupUi(this);
    QFile file("C://Users/Lisanus/Desktop/projects/Lisa/jewelery.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

SecondWindow::~SecondWindow()
{
    delete ui;
}

void SecondWindow::on_pushButton_clicked()
{
    ui->label->setText(ui->listWidget->currentItem()->text());
}

void SecondWindow::on_pushButton_2_clicked()
{
    hide();
    Menu_user menu_window;
    menu_window.setModal(true);
    menu_window.exec();
}
